package com.alipay.trade.model.hb;

/**
 * Created by liuyangkly on 15/8/27.
 */
public enum ExceptionInfo {
    /**
     * He printer exception info.
     */
    HE_PRINTER // 打印机异常；

    ,
    /**
     * He scaner exception info.
     */
    HE_SCANER  // 扫描枪异常；

    ,
    /**
     * He other exception info.
     */
    HE_OTHER   // 其他硬件异常
}
